#!/bin/bash

VENV_DIRNAME=./project-venv

if [ -d "$VENV_DIRNAME" ]; then
  echo ""
  printf "VE (virtual environment) directory ($VENV_DIRNAME) already exists.\n" 
else
  echo ""
  printf "Creating VE (virtual environment): $VENV_DIRNAME:"
  python3 -m venv project-venv
  source ./project-venv/bin/activate  

  echo ""
  printf "\n  1) Installing from requirements.txt...\n\n" 
  pip3 install -r requirements.txt
  printf "\n  2) Confirming Django version for this VE is: "
  python3 -m django --version
fi

printf "\n  Usage: Enter the VE using:  source $VENV_DIRNAME/bin/activate" 
printf "\n         Exit the VE using:   deactivate\n\n" 

