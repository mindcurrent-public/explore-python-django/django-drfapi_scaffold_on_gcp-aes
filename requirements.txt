django               # Along with...
djangorestframework  # The de-facto REST API for Django
markdown             # Markdown support for the browsable API.
django-filter        # Additional filtering support
