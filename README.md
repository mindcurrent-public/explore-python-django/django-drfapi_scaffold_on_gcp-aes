# django-drfapi_scaffold_on_gcp-aes

Deploy a basic (but extensible) Django + DRFAPI (Django Rest Framework API) configuration (using the default built-in SQLite database) from a localhost instance onto a secure VPC configuration running in an AES instance on GCP.

### Deployment instructions:

Developed per issue: [outline: Streamlined SOP for localhost deployments of Django ](https://gitlab.com/p7350/ehp/jarvish-the-clone/-/issues/3)


1. Clone the [this repo](https://gitlab.com/p7350/ehp/jarvish-the-clone) to localhost:
   ```
   git clone https://gitlab.com/p7350/ehp/jarvish-the-clone.git
   cd ./jarvish-the-clone
   ```
2. Initialize the `create-react` build dependencies:
   ```
   cd ./frontend
   yarn
   ```
3. Build the `create-react` files required by the Django instance:
   ```
   yarn build
   ```
4. Initialize the Django build dependencies:
   ```
   cd ..
   ./setup.project.venv.bash
   source ./project-venv/bin/activate
   ```
5. Stand up the Django server instance (in a separate terminal): 
   ```
   python3 manage.py runserver # You will see warnings to apply the standard Django migrations
   ^c # to stop the server and apply the migrations.
   python3 manage.py migrate
   python3 manage.py runserver 
   ```
6. Stand up the `create-react` SPA instance (in the original terminal):
   ```
   yarn start
   ```

### Additional Resources
* Guidance (from GCP):
  * [Setting up a Python development environment](https://cloud.google.com/python/docs/setup)
    * which concludes with a "quick start": [Deploy a Python app to App Engine...](https://cloud.google.com/appengine/docs/standard/python3/create-app)

